var config = require('./dbconfig');
const sql = require('mssql');

async function CheckIn(stUserID, biEid) {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request()
            .input('param_user', sql.VarChar, stUserID)
            .input('param_emp', sql.Int, biEid)
            //.query("SELECT * FROM gen.TU WHERE stUserID = @param_user AND stUserPwd = @param_pass");
            .query("EXEC hrd.sp_Abs_I 'IN',@param_emp,'@param_user'");
        return user.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function CheckOut(stUserID, biEid) {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request()
            .input('param_user', sql.VarChar, stUserID)
            .input('param_emp', sql.Int, biEid)
            //.query("SELECT * FROM gen.TU WHERE stUserID = @param_user AND stUserPwd = @param_pass");
            .query("EXEC hrd.sp_Abs_I 'OUT',@param_emp,'@param_user'");
        return user.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function GetAbsenList(stCid, biEid) {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request()
            .input('param_client', sql.VarChar, stCid)
            .input('param_emp', sql.Int, biEid)
            //.query("SELECT * FROM gen.TU WHERE stUserID = @param_user AND stUserPwd = @param_pass");
            .query("SELECT stTgl, biAbsID, InOrOut, stJam FROM hrd.ft_TEAbs(@param_client, @param_emp)");
        return user.recordsets;
    } catch (error) {
        console.log(error);
    }
}
``

module.exports = {
    CheckIn: CheckIn,
    CheckOut: CheckOut,
    GetAbsenList: GetAbsenList
}