const config = {
    user: 'sa',
    password: 'A#nd007.',
    server: '128.199.182.174',
    database: 'test',
    options: {
        trustetdconnection: true,
        enableAritAort: true,
        instancename: 'MSSQLSERVER',
        encrypt: false,
        trustServerCertificate: true
    },
    port: 8080
}

module.exports = config;