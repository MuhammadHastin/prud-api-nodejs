var config = require('./dbconfig');
const sql = require('mssql');
``
async function getLogin() {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request().query("SELECT * FROM gen.TU");
        return user.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function postLogin(stUserID, stUserPwd) {
    try {
        let pool = await sql.connect(config);
        let user = await pool.request()
            .input('param_user', sql.VarChar, stUserID)
            .input('param_pass', sql.VarChar, stUserPwd)
            //.query("SELECT * FROM gen.TU WHERE stUserID = @param_user AND stUserPwd = @param_pass");
            .query("exec gen.sp_Get_stUserID_From_stUserIDandPwd @param_user,@param_pass");
        return user.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function CleaningServiceList() {
    try {
        let pool = await sql.connect(config);
        let cleaningservice = await pool.request()
            .query("SELECT te.stCid, tc.stCName, te.biEid, te.stEName, te.stECode, tecs.stCSJobID, tecsJob.stCSJobName, tecs.imgFile1, tecs.imgFile2, tecs.imgFile3 FROM hrd.TECS tecs INNER JOIN hrd.TE te ON tecs.biEid = te.biEid INNER JOIN gen.TC tc ON te.stCid = tc.stCid LEFT OUTER JOIN hrd.TCSjob tecsJob ON tecs.stCSJobID = tecsJob.stCSJobID");
        return cleaningservice.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getClient() {
    try {
        let pool = await sql.connect(config);
        let client = await pool.request()
            .query("SELECT stCid, stCName FROM gen.ft_List_Client1()");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getKaryawanByClient(stCid) {
    try {
        let pool = await sql.connect(config);
        let client = await pool.request()
            .input('param_client', sql.VarChar, stCid)
            .query("SELECT biEid, stEName, stECode FROM gen.ft_List_ECS_From_stCid(@param_client)");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getAktifitasKaryawan(dtCS, stCid, biEid) {
    console.log(dtCS, stCid, biEid)
    try {
        let pool = await sql.connect(config);
        let client = await pool.request()
            .input('param_cs', sql.VarChar, dtCS)
            .input('param_cid', sql.VarChar, stCid)
            .input('param_eid', sql.VarChar, biEid)
            .query("exec hrd.sp_CS_L @param_cs,@param_cid,@param_eid");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}


async function updateAktifitasKaryawan(dtCS, stCid, biEid, biECS, stFileName1, stFileName2, stFileName3, stUserID) {
    try {
        console.log(dtCS, stCid, biEid, biECS, stFileName1, stFileName2, stFileName3, stUserID);
        let pool = await sql.connect(config);
        let client = await pool.request()
            .input('param_date', sql.VarChar, dtCS)
            .input('param_cid', sql.VarChar, stCid)
            .input('param_eid', sql.Int, biEid)
            .input('param_csid', sql.Int, biECS)
            .input('param_imgname1', sql.VarChar, stFileName1)
            .input('param_imgname2', sql.VarChar, stFileName2)
            .input('param_imgname3', sql.VarChar, stFileName3)
            .input('param_userid', sql.VarChar, stUserID)
            .query("exec hrd.sp_CS_U_Img @param_date, @param_cid, @param_eid, @param_csid, @param_imgname1, @param_imgname2, @param_imgname3, @param_userid");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getReport(dtCS, stCid, Spv, OMgr) {
    try {
        console.log(dtCS, stCid, Spv, OMgr);
        let pool = await sql.connect(config);
        let client = await pool.request()
            .input('param_date', sql.VarChar, dtCS)
            .input('param_cid', sql.VarChar, stCid)
            .input('param_spv', sql.VarChar, Spv)
            .input('param_mgr', sql.VarChar, OMgr)
            .query("SELECT * FROM hrd.ft_Rpt_CS (@param_date, @param_cid, @param_spv, @param_mgr)");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}

async function getListPekerjaan() {
    try {
        //console.log(dtCS, stCid, Spv, OMgr);
        let pool = await sql.connect(config);
        let client = await pool.request()
            // .input('param_date', sql.VarChar, dtCS)
            // .input('param_cid', sql.VarChar, stCid)
            // .input('param_spv', sql.VarChar, Spv)
            // .input('param_mgr', sql.VarChar, OMgr)
            .query("SELECT * FROM hrd.ft_List_CSJob()");
        return client.recordsets;
    } catch (error) {
        console.log(error);
    }
}
module.exports = {
    getLogin: getLogin,
    postLogin: postLogin,
    CleaningServiceList: CleaningServiceList,
    getClient: getClient,
    getKaryawanByClient: getKaryawanByClient,
    getAktifitasKaryawan: getAktifitasKaryawan,
    updateAktifitasKaryawan: updateAktifitasKaryawan,
    getReport: getReport,
    getListPekerjaan: getListPekerjaan
}