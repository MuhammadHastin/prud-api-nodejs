var Login = require('./login');
var Db = require('./dboperations');
const dboperations = require('./dboperations');
const absen = require('./absen');
const fileUpload = require('express-fileupload');

var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
const { user } = require('./dbconfig');
var app = express();
app.use(fileUpload());
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
var router = express.Router();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use('/api', router);


router.use((request, response, next) => {
    console.log('middleware');
    next();
})

router.route('/login').get((request, response) => {
    Db.getLogin().then((data) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: data[0]
            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/postlogin/:userid/:password').get((request, response) => {
    Db.postLogin(request.params.userid, request.params.password).then((data) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: data[0][0]
            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }

    });
})

router.route('/cleaningservice/list').get((request, response) => {
    Db.CleaningServiceList().then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/client/list').get((request, response) => {
    Db.getClient().then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/karyawanbyclient/list/:stCid').get((request, response) => {
    Db.getKaryawanByClient(request.params.stCid).then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/aktifitaskaryawan/list/:cs/:cid/:eid').get((request, response) => {
    Db.getAktifitasKaryawan(request.params.cs, request.params.cid, request.params.eid).then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})



router.route('/update/aktifitaskaryawan/:dtCS/:stCid/:biEid/:biECS/:stFileName1/:stFileName2/:stFileName3/:stUserID').get((req, response) => {
    // console.log("test", req)

    Db.updateAktifitasKaryawan(
        req.params.dtCS,
        req.params.stCid,
        req.params.biEid,
        req.params.biECS,
        req.params.stFileName1,
        req.params.stFileName2,
        req.params.stFileName3,
        req.params.stUserID).then((datas) => {
            //console.log(result);
            if (response.statusCode == 200) {
                return response.json({
                    code: response.statusCode,
                    message: "Success",
                    values: datas[0]

                });
            } else {
                return response.json({
                    code: response.statusCode,
                    message: "Failed",
                });
            }
        })
})

router.route('/report/:dtCS/:stCid/:Spv/:OMgr').get((req, response) => {
    // console.log("test", req)

    Db.getReport(
        req.params.dtCS,
        req.params.stCid,
        req.params.Spv,
        req.params.OMgr).then((datas) => {
            //console.log(result);
            if (response.statusCode == 200) {
                return response.json({
                    code: response.statusCode,
                    message: "Success",
                    values: datas[0]

                });
            } else {
                return response.json({
                    code: response.statusCode,
                    message: "Failed",
                });
            }
        })
})

router.route('/cleaningservice/pekerjaan/list').get((req, response) => {
    // console.log("test", req)

    Db.getListPekerjaan().then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/absen/in/:stUserID/:biEid').get((req, response) => {
    // console.log("test", req)

    absen.CheckIn(
        req.params.stUserID,
        req.params.biEid
    ).then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/absen/out/:stUserID/:biEid').get((req, response) => {
    // console.log("test", req)

    absen.CheckOut(
        req.params.stUserID,
        req.params.biEid
    ).then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})

router.route('/absen/list/:stCid/:biEid').get((req, response) => {
    // console.log("test", req)

    absen.GetAbsenList(
        req.params.stCid,
        req.params.biEid
    ).then((datas) => {
        //console.log(result);
        if (response.statusCode == 200) {
            return response.json({
                code: response.statusCode,
                message: "Success",
                values: datas[0]

            });
        } else {
            return response.json({
                code: response.statusCode,
                message: "Failed",
            });
        }
    })
})



var port = process.env.PORT || 8090;
app.listen(port);
console.log('Order API is runnning at ' + port);

app.use(
    '/api-docs',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument)
);



